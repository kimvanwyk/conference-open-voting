FROM registry.gitlab.com/kimvanwyk/python3-poetry-container:latest

COPY ./conference_open_voting/ /app/
COPY run.sh /app

VOLUME /storage

ENV STORAGE_DIR=/storage

ENTRYPOINT ["bash", "run.sh"]
