from collections import defaultdict, Counter
import os

from django.db.models import Count
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from dotenv import load_dotenv

from .models import Choice, Question, QuestionOrder, CurrentQuestion, Voter, Vote

load_dotenv()


def vote(request):
    error_message = None
    question = get_object_or_404(CurrentQuestion).question
    if question is None:
        return HttpResponse(
            f"<h3>No question is currently being voted on. Refresh this page to check again.</h3>"
        )

    if request.method == "POST":
        identifier = request.POST.get("identifier")
        voter = Voter.objects.filter(
            identifier_text=identifier.lower().split("mdc")[-1].strip()
        ).first()
        if not voter:
            return HttpResponse(
                f'<h3>Registration number {identifier} is not indicated as a voting delegate. <a href="/">Click here</a> to try to vote again if there was an error in your entered registration number.</h3>'
            )
        choice_id = request.POST.get("choice")
        if choice_id is None:
            error_message = "Please select a voting option"
        else:
            v = Vote.objects.filter(question=question, voter=voter).all()
            v.delete()
            v = Vote(question=question, voter=voter, choice_id=choice_id)
            v.save()
            return HttpResponseRedirect(
                reverse(
                    "voting:vote_result",
                    args=(question.id,),
                )
            )

    if error_message or request.method == "GET":
        return render(
            request,
            "voting/vote.html",
            {"question": question, "error_message": error_message},
        )


def vote_result(request, question_id=None):
    if question_id:
        question = get_object_or_404(Question, id=question_id)
    else:
        question = get_object_or_404(CurrentQuestion).question
    return render(request, "voting/vote_result.html", {"question": question})


def results(request):
    results = []
    questions = QuestionOrder.objects.order_by("-id").all()
    for qo in questions:

        total_votes = len(Vote.objects.filter(question=qo.question).all())
        if total_votes > 0:
            votes = (
                Vote.objects.filter(question=qo.question)
                .values("choice__choice_text")
                .annotate(dcount=Count("choice"))
                .order_by("choice__id")
            )
            d = {
                "title": qo.question.title_text,
                "description": qo.question.description_text,
                "choices": [],
            }
            for v in votes:
                d["choices"].append(
                    {
                        "choice": v["choice__choice_text"],
                        "count": v["dcount"],
                        "percentage": f"{v['dcount']/(total_votes * 1.0) * 100:0.2f}",
                    }
                )
            d["total_votes"] = total_votes
            results.append(d)
    return render(request, "voting/results.html", {"results": results})
