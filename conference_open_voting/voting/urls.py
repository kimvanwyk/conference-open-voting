from django.urls import path

from . import views

app_name = "voting"
urlpatterns = [
    path("", views.vote, name="vote"),
    path("vote_result/<int:question_id>", views.vote_result, name="vote_result"),
    path(
        "results",
        views.results,
        name="results",
    ),
]
