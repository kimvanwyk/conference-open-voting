from django.contrib import admin
from solo.admin import SingletonModelAdmin

from .models import Choice, Question, QuestionOrder, CurrentQuestion, Voter


class QuestionOrderAdmin(admin.ModelAdmin):
    ordering = ["id"]


class QuestionAdmin(admin.ModelAdmin):
    ordering = ["id"]


admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, admin.ModelAdmin)
admin.site.register(CurrentQuestion, SingletonModelAdmin)
admin.site.register(Voter, admin.ModelAdmin)
