import datetime
import random

from django.db import models, transaction, IntegrityError
from django.utils import timezone

from solo.models import SingletonModel


class Voter(models.Model):
    identifier_text = models.CharField("Identifier", max_length=50)

    def __str__(self):
        return self.identifier_text


class Choice(models.Model):
    choice_text = models.CharField(max_length=100)

    def __str__(self):
        return self.choice_text


class Question(models.Model):
    title_text = models.CharField(max_length=100)
    description_text = models.CharField(max_length=1000, null=True, blank=True)
    choices = models.ManyToManyField(Choice)

    def __str__(self):
        return self.title_text


class QuestionOrder(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    def __str__(self):
        return self.question.title_text


class CurrentQuestion(SingletonModel):

    question = models.ForeignKey(
        Question, null=True, blank=True, on_delete=models.CASCADE
    )

    def __str__(self):
        return self.question.title_text if self.question else "No current question"

    def save(self, *args, **kwds):
        if (
            self.question
            and not QuestionOrder.objects.filter(question=self.question).exists()
        ):
            q = QuestionOrder(question=self.question)
            q.save()
        super().save(*args, **kwds)

    class Meta:
        verbose_name = "Current Question"


class Vote(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    voter = models.ForeignKey(Voter, on_delete=models.CASCADE)
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE)
